package com.apirest.models;

public class UserModel {

    private String userId;

    public UserModel() {
    }

    public UserModel(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }
}
